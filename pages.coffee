###
TODO: Make this watching.
may not be possibleon linux with node :(
###

# We need the Handlebars for the templating
Handlebars = require( 'handlebars' )
# We need fs for the file writing and tree searching
fs = require( 'fs' )

# Read in the template file, and compile it to a handlebars template.
data = fs.readFileSync( 'tmpl.html', 'utf-8' )
global.tmpl = Handlebars.compile( data )

readTree = ( dir ) ->
	fs.readdir( dir, ( err, files ) ->
		if err? then console.log( err )
		files.map( ( file ) ->
			filePath = dir + '/' + file
			if file == "index.json"
				fs.writeFile( dir + '/' +  "index.html", global.tmpl( JSON.parse ( fs.readFileSync( filePath ) ) ), ( err ) ->
					if err? then console.log( err ) )
			else
				fs.stat( filePath, ( err, stats ) ->
					if stats.isDirectory()
						readTree( filePath ) ) ) )

readTree( './' )

###
global.blogData = JSON.parse( fs.readFileSync( 'blog/index.json', 'utf-8' ) )
global.projectsData = JSON.parse( fs.readFileSync( 'projects/index.json', 'utf-8' ) )
global.homeData = JSON.parse( fs.readFileSync( 'index.json', 'utf-8' ) )

# here's where we write it to their respective file locations:
fs.writeFile( 'blog/index.html', global.tmpl( global.blogData ), ( err ) ->
	if err? then ( console.log( err ) ) )

fs.writeFile( 'projects/index.html', global.tmpl( global.projectsData ), ( err ) ->
	if err? then ( console.log( err ) ) )

fs.writeFile( 'index.html', global.tmpl( global.homeData ), ( err ) ->
	if err? then ( console.log( err ) ) )
###
