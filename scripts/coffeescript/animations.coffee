# Sizzle is a library that provides only jQuery's selector function without any
# of the overhead of jQuery.
$ = Sizzle
tw = TweenLite

# this
redirect = ( target ) ->
	window.location = target

# When the entire document is loaded (prevents trying to bind event handlers to
# elements that don't exist yet)
window.onload = ->

	# This smooth-transitions in the body.
	tw.from( $( 'main' ), 0.4, { opacity: 0, marginTop: "+=2em" } )

	# For every <a> in the header (I use a map instead of a for loop for brevity)
	$( 'header a' ).map( ( el ) ->

		# strobeOn is a "timeline", or animation sequence, that initially flashes the
		# background to straight white and the text to black over the span of 0.1
		# seconds, and then dims the background back down to #DDD over the span of
		# 0.4 seconds.
		strobeOn = new TimelineLite( { paused: true } )
		strobeOn.add( tw.to( el, 0.1, { color: "black", backgroundColor: "white" } ) )
		strobeOn.add( tw.to( el, 0.4, { color: "black", backgroundColor: "#DDD" } ) )

		# On mouseover, play strobeOn from the beginning.
		el.addEventListener( 'mouseover', ->
			strobeOn.restart() )
		# And, on mouseout, stop playing this if it's playing, then fade the <a> back
		# to it's original appearance.
		el.addEventListener( 'mouseout', ->
			strobeOn.pause()
			tw.to( el, 0.5, { backgroundColor: "none", color: "white" } ) )
		# On mousedown, change the background to #026ba3 and the text back to white.
		el.addEventListener( 'mousedown', ->
			strobeOn.pause()
			tw.to( el, 0.1, { backgroundColor: "#026ba3", color: "white" } ) )
		# And, on mouseup, undo the previous change.
		el.addEventListener( 'mouseup', ->
			strobeOn.pause()
			tw.to( el, 0.1, { backgroundColor: "#DDD", color: "black" } ) )

		el.addEventListener( 'click', ( e ) ->
			e.preventDefault()
			target = el.href
			tw.to( $( 'main' ), 0.2, { opacity: 0, onComplete: redirect, onCompleteParams: [ target ] } ) ) )
